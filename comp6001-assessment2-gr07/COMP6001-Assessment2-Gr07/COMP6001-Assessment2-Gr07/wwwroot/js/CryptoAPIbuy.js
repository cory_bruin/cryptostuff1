﻿$.getJSON("https://api.coinmarketcap.com/v1/ticker/?&limit=10", function (json) {
    var tableHeaders = ["Rank", "Symbol", "Name", "Value", ""];
    var table = document.createElement("table");
    table.setAttribute('class', 'table table-hover');
    for (var i = 0; i < 1; i++) {
        var row = table.insertRow(-1);
        var tableHeader1 = row.insertCell(-1);
        tableHeader1.appendChild(document.createTextNode(tableHeaders[0]));
        var tableHeader2 = row.insertCell(-1);
        tableHeader2.appendChild(document.createTextNode(tableHeaders[1]));
        var tableHeader3 = row.insertCell(-1);
        tableHeader3.appendChild(document.createTextNode(tableHeaders[2]));
        var tableHeader4 = row.insertCell(-1);
        tableHeader4.appendChild(document.createTextNode(tableHeaders[3]));
        var tableHeader5 = row.insertCell(-1);
        tableHeader5.appendChild(document.createTextNode(tableHeaders[4]));
    }
    for (var i = 0; i < json.length; i++) {
        var row = table.insertRow(-1);
        var rankCell = row.insertCell(-1);
        rankCell.appendChild(document.createTextNode(json[i].rank));
        var symbolCell = row.insertCell(-1);
        symbolCell.appendChild(document.createTextNode(json[i].symbol));
        var nameCell = row.insertCell(-1);
        nameCell.appendChild(document.createTextNode(json[i].name));
        var priceCell = row.insertCell(-1);
        priceCell.appendChild(document.createTextNode('$' + parseFloat(json[i].price_usd).toFixed(2)));
        var buyButtonCell = row.insertCell(-1);
        var buyButton = document.createElement("button");
        buyButton.setAttribute("class", "buyClick btn btn-success");
        buyButton.innerHTML = "Purchase";
        buyButton.setAttribute("data-sym", json[i].symbol);
        buyButton.setAttribute("data-price", json[i].price_usd);
        buyButton.setAttribute("data-toggle", "modal");
        buyButton.setAttribute("data-target", "#myModal");
        buyButtonCell.appendChild(buyButton);
    }
    $("#cryptoTable").append(table);
    $("body").on("keyup", ".purchaseQTY", function () {
        var coinvalue = $(".purchasePrice").val();
        var value = $(this).val();
        $('.purchaseValue').val('$' + parseFloat(coinvalue * value).toFixed(2));
    });
});