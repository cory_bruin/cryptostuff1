# The Learning and Demonstration of GUI practice: A bed of nails, a sleep of gold.
## Project Overview:
The project that we've been given is very freeing, especially in the sense that we get to choose the API that we want use. Our group (Cory Bruin and James Beck) shares an common interest in cryptocurrencies. With that in mind, Cory and James can work on a passion project for this assignment and as such, we've chosen to utilise a cryptocurrency monitoring websites API (that website being coinmarketcap.com).

---
## Team Roles:
* Cory Bruin: Senior Group Associate
* James Beck: Project Leader

## Allocated Sections:
##### Cory Bruin
* Portfolio Tracking page - Allowing users will track their investments over time.
* Add/Delete/Manage function - Saves/Modifies users inputted information into a database.
* Coinmarketcap API for extracting cryptocurrency information.
* Tracking calculations and functions
* SQL database work
##### James Beck
* Index Page - The main landing page for our website.
* Coinmarketcap API displaying top 20 Cryptocurrencies
* Login function - for user accounts and allowing access to the personalised Portfolio Tracking page.
* SQL database work
---
## Resources:
There are many, many resources that Cory and James are looking to use to help them their project. Listed below is the proposed, and not necessarily final, technology stack:  

**Technology**  
* Visual Studio 2017 will be used to aid in the development of an effectively design ASP.Net website. We had other options, like visual studio code, but our choice eventually came to this because we have both had experience working in both the visual studio 2017 and visual studio code environments, and our preference for project management goes to the former. On a final note, Visual Studio 2017 allows us to integrate nuget packages and other languages fantastically, a role that in our opinion visual studio code struggles with.
* ASP.net is the framework we will use to develop this website. This is both the preferred choice of Cory and James, as we were able to select this option at the beginning of our course. ASP.net is a website framework, that allows the integration of C# code into HTML files by using files that have a .cshtml suffix. ASP.Net also uses a system of design called MVC (model view class), a method of designing websites that allows for a lot more flexibility, reduces code complexity, decreases dependencies, and allows us to reuse code more often.
* Docker eliminates the “it works on my machine” problem once and for all. It also packages dependencies with your apps in Docker containers for portability and predictability during development, testing, and deployment.
* Coinmarketcap API is our datasource for this assessment as they provide an accurate JSON string of up to date cryptocurrency values. The API, similar to other API, exports a multi-dimensional array, the first containing the array values of a single coin, the second containing the information of that coin. Our choice of the Coinmarketcap API is because both Cory and James have a current interest in cryptocurrency, so our attention to this project is expected to greatly increase; an important position to take considering our lacking group size.

**Additional Resources**  
* Google will be used for research and develop while completing this project. It is in essence the largest library the world has ever known. With that availability of information, we can query anything that we don't know off the top of our heads, or we want to understand in further detail. As per usual, we'll simply be using the search bar.
* Microsoft Docs is a useful knowledge base containing helpful information and tutorials regarding ASP.Net framework. Microsoft Docs have tutorials, specifically for using API's (though not the API that we're using). However the relevance of this information is nearly invaluable to our team.
* StackOverflow is a community based question and answer forum that is particularly useful when trying solve programming problems. This will be used for technical help on questions that are usually too difficult or specific for general IT blogs that we might find with google searches, or the specificity of Microsoft Documents.
* Jeff Kranenburg is an excellent guy and a fantastic tutor that will be of great assistance throughout this project. He's our first point of contact in class, and his evaluation is a great way to make sure that we're making the correct steps to put forward the best project possible.
* Moodle is a learning resource that contains the project information and marking schedule used in this assessment. In any normal case, there would be a variety of supplementary information for the project, and paper, that we're currently doing. However, that is instead stored on a different website. Because of this, our use of moodle is likely limited to the marking schedules.
* To-bcs.net is the Toi Ohomai Institute of Technology's tutorial website for the level 6 Diploma in applied computer course. This website is purpose built for IT students, and contains the course information for our paper. In regards to API, it contains information on implementing a JSON string with an ASP.Net website in the *week 3* section. This is a very valuable resource to use.

**Communication**  
* Slack will be our main source of communication throughout this assessment. Slack will also be used to communicate updates, organisation, and effort to our tutor, Jeff. In addition, Slack is a program that recently we've found has the ability to add apps. As such, we've added an app for trello, though we may not able to use this resource effectively until the second part of the project.
* Trello will assist our task delegation and tracking progress. We've split our trello up into a list of To Do, In Progress, Checked (both by the team, and by the tutor (which may not always be possible)), and Submitted. Trello is essentially the visualisation of our project progress.

**Version Control**  
* Bitbucket will control and manage all versions of our code and allow us to work simultaneously on different sections of the project. Furthermore, Bitbucket is also the online repository that both Cory and James are most accustom to. To be frank, that is the only differentiator between Bitbucket and GitHub.

**Wireframes**
* Microsoft Paint will be used to create detailed wireframes showing a visual representation of our intended website layout. The preference to use Microsoft Paint is due to the ease with which we can layout a website, and still retain it's quality. The former is why we didn't choose visio, the latter is why we didn't choose drawing by hand.
---
